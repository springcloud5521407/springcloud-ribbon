package com.qytest.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;

/**
 * 自定义规则类
 */
public class MySelfRule {
    @Bean
    public IRule myRule(){
        return  new RandomRule();
    }
}
