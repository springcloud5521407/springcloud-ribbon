package com.qytest.springcloud;

import com.qytest.myrule.MySelfRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @author qy
 * @date 2022年07月13日 16:46
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient
@RibbonClient(name="CLOUD-PROVIDER-PAYMENT",configuration = MySelfRule.class)
public class CloudConsumerOrder {
    public static void main(String[] args) {
        SpringApplication.run(CloudConsumerOrder.class, args);
    }
}